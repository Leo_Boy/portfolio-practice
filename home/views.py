from django.shortcuts import render, HttpResponse
from django.contrib import messages


def home(request):
    return render(request, 'home/home.html')


def contact(request):
    if request.method == "POST":
        firstname = request.POST.get('inputname')
        lastname = request.POST.get('inputlastname')
        email = request.POST.get('inputemail')
        phone = request.POST.get('inputphone')
        subject = request.POST.get('inputsubject')
        message = request.POST.get('inputMessage')
        # can add send email feature here
        messages.info(request, "Form has been submitted")
    return render(request, 'home/contact.html')


def portfolio(request):
    return render(request, 'home/portfolio.html')


def photo(request):
    return render(request,'home/photos.html')